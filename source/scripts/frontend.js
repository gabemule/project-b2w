(function($) {

    var app = function() {
        this.body = $('body');

        var destionationsTabs = function() {
            $('.mosaic-options li').click(function() {
                $('.mosaic-options li.active').removeClass('active');
                $(this).addClass('active');
                $('.mosaic-view li.active').removeClass('active');
                var dataMosaic = $(this).attr('data-mosaic');
                $('.mosaic-view li[data-mosaic=' + dataMosaic + ']').addClass('active');
            });
        };

        var reviewsSlider = function() {
            $('.slider').slick({
                dots: true,
                autoplay: true,
                autoplaySpeed: 7000,
                speed: 500,
            });
        };

        var backToTop = function() {
            $('.btn-back').click(function(e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        };

        var mobileMenu = function() {
            $('.mobile-btn').click(function() {
                $('header .menu ul').slideToggle();
            });


        };

        var mobileSelectMenu = function() {
            // Create the dropdown base
            $("<select />").appendTo(".mosaic-options");

            // Create default option "Selecione..."
            $("<option />", {
                "selected": "selected",
                "value": "",
                "text": "Selecione..."
            }).appendTo(".mosaic-options select");

            // Populate dropdown with mosaic-options items
            $(".mosaic-options li").each(function() {
                var el = $(this);
                $("<option />", {
                    "value": el.attr('data-mosaic'),
                    "text": el.text()
                }).appendTo(".mosaic-options select");
            });

            $('.mosaic-options select').on('change', function() {
                var data = $(this).val();
                $('.mosaic-view li.active').removeClass('active');
                $('.mosaic-view li[data-mosaic=' + data + ']').addClass('active');
            });

        };

        //RUN FUNCTIONS
        var run = function() {
            destionationsTabs();
            reviewsSlider();
            backToTop();
            mobileMenu();
            mobileSelectMenu();
        };

        run();
    };

    $(function() {
        app();
    });
})(jQuery);