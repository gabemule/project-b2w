<footer>
    <div class="container">
        
        <div class="row">
            
            <div class="contacts col-xs-6 col-sm-6 col-md-4">
                <h3>Contatos</h3>
                <br>
                <div class="row">
                    <div class="col-md-5">
                        <p>
                            Rua Nome da Rua, 300
                        </p>
                        <p>
                            Rio de Janeiro, RJ
                        </p>
                    </div>
                    <div class="col-md-5">
                        <p>
                            +55 21 1234-5678
                        </p>
                        <p>
                            contato@lojasamericanas.net
                        </p>
                    </div>
                </div>
            </div>
            
            <div class="follow-us col-xs-6 col-sm-6 col-md-3">
                <h3>Siga-nos</h3>
                <br>
                <ul>
                    <li class="icon pinterest"></li>
                    <li class="icon twitter"></li>
                    <li class="icon facebook"></li>
                </ul>
            </div>
            
            <div class="about-us col-xs-12 col-sm-12 col-md-4">
                <h3>Sobre</h3>
                <br>
                <p>
                    Esse modelo é totalmente fictício e foi para nos ajudar a testar recursos de desenvolvimento de websites responsivos. Esse modelo é totalmente fictício e foi para nos ajudar a testar recursos de desenvolvimento de websites responsivos.
                </p>
            </div>
            
            <div class="sub-footer col-xs-12 col-sm-12 col-md-12">
                <ul>
                    <li>Destinos</li>
                    <li>Planos</li>
                    <li>Depoimentos</li>
                    <li>Contato</li>
                </ul>
                <div class="copy">
                    &copy; 2016 Lojas Americanas
                </div>
                <div class="btn-back">
                    <p>
                        Voltar ao topo
                    </p>
                    <div class="back-to-top">
                        <span></span>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</footer>