<!-- San francisco -->
<li class="active" data-mosaic="san-francisco">
    
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-1.png" 
            srcset="assets/images/card-1@2x.png 2x, assets/images/card-1@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-2.png" 
            srcset="assets/images/card-2@2x.png 2x, assets/images/card-2@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-3.png" 
            srcset="assets/images/card-3@3x.png 2x, assets/images/card-3@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-4.png" 
            srcset="assets/images/card-4@3x.png 2x, assets/images/card-4@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-5.png" 
            srcset="assets/images/card-5@2x.png 2x, assets/images/card-5@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-6.png" 
            srcset="assets/images/card-6@2x.png 2x, assets/images/card-6@3x.png 3x">
    </div>

</li>

<!-- Paris -->
<li data-mosaic="paris">
    
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-4.png" 
            srcset="assets/images/card-4@3x.png 2x, assets/images/card-4@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-5.png" 
            srcset="assets/images/card-5@2x.png 2x, assets/images/card-5@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-6.png" 
            srcset="assets/images/card-6@2x.png 2x, assets/images/card-6@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-1.png" 
            srcset="assets/images/card-1@2x.png 2x, assets/images/card-1@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-2.png" 
            srcset="assets/images/card-2@2x.png 2x, assets/images/card-2@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-3.png" 
            srcset="assets/images/card-3@3x.png 2x, assets/images/card-3@3x.png 3x">
    </div>

</li>

<!-- London -->
<li data-mosaic="london">
    
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-5.png" 
            srcset="assets/images/card-5@2x.png 2x, assets/images/card-5@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-6.png" 
            srcset="assets/images/card-6@2x.png 2x, assets/images/card-6@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-3.png" 
            srcset="assets/images/card-3@3x.png 2x, assets/images/card-3@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-4.png" 
            srcset="assets/images/card-4@3x.png 2x, assets/images/card-4@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-1.png" 
            srcset="assets/images/card-1@2x.png 2x, assets/images/card-1@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-2.png" 
            srcset="assets/images/card-2@2x.png 2x, assets/images/card-2@3x.png 3x">
    </div>

</li>

<!-- Sidney -->
<li data-mosaic="sidney">
    
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-1.png" 
            srcset="assets/images/card-1@2x.png 2x, assets/images/card-1@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-4.png" 
            srcset="assets/images/card-4@3x.png 2x, assets/images/card-4@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-3.png" 
            srcset="assets/images/card-3@3x.png 2x, assets/images/card-3@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-2.png" 
            srcset="assets/images/card-2@2x.png 2x, assets/images/card-2@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-5.png" 
            srcset="assets/images/card-5@2x.png 2x, assets/images/card-5@3x.png 3x">
    </div>
    <div class="col-xs-6 col-sm-6 col-md-4">
        <img 
            class="img-responsive"
            src="assets/images/card-6.png" 
            srcset="assets/images/card-6@2x.png 2x, assets/images/card-6@3x.png 3x">
    </div>

</li>