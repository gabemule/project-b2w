<header>
    <div class="container">
        <div class="logo">
            <img 
                class="img-responsive"
                src="assets/images/logo-americanas-900-x-225-fw.png"
                srcset="assets/images/logo-americanas-900-x-225-fw@2x.png 2x, assets/images/logo-americanas-900-x-225-fw@3x.png 3x">
        </div>
        
        <div class="menu">
            <div class="mobile-btn">
                <img 
                    src="assets/images/hamburger-menu.png"
                    srcset="assets/images/hamburger-menu@2x.png 2x, assets/images/hamburger-menu@3x.png 3x">
            </div>
            <ul>
                <li>Destinos</li>
                <li>Planos</li>
                <li>Depoimentos</li>
                <li>Contatos</li>
                <li class="highlight">Faça um tour</li>
            </ul>
        </div>
    </div>
</header>