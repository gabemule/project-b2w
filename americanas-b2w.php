<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Americanas</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- THIS COMMENT TAG IS NEEDED FOR GRUNT BUILD PROCESS -->
    <!-- build:css assets/css/frontend.min.css -->
    <!-- bower:css -->
    <link rel="stylesheet" href="bower_components/slick-carousel/slick/slick.css" />
    <!-- endbower -->
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- endbuild -->
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,700" rel="stylesheet">
    
    <script src="assets/vendor/modernizr.js"></script>
</head>
<body>

<!-- CONTENT -->
<!-- CONTENT -->

    <?php include "/b2w/includes/header.php"; ?>
    
    <div class="highlight-banner">
        <div class="content">
            <h2>Nessas férias, escolha o produto certo para você.</h2>
            <span>Participe da nossa Mailing List.</span>
            <form class="" action="index.html" method="post">
                <input type="text" placeholder="Seu e-mail">
                <input type="button" name="name" value="Saiba Mais">
            </form>
            <div class="category-btn">
                <ul>
                    <li><span class="icon plane"></span></li>
                    <li><span class="icon wallet"></span></li>
                    <li><span class="icon case"></span></li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="destinations">
        <div class="container">
            <h2>Destinos</h2>
            <div class="mosaic">
                
                <ul class="mosaic-options">
                    <li class="active" data-mosaic="san-francisco">São Francisco</li>
                    <li data-mosaic="paris">Paris</li>
                    <li data-mosaic="london">Londres</li>
                    <li data-mosaic="sidney">Sidney</li>
                </ul>
                
                <ul class="mosaic-view">
                    
                    <?php include "includes/mosaic.php"; ?>
                    
                </ul>
            </div> <!-- .mosaic -->
        </div>
    </div> <!-- .destinations -->
    
    <div class="reviews">
        <div class="container">
            
            <div class="slider">
                <div class="slide">
                    <div class="content">
                        <img 
                            src="assets/images/foto.png"
                            srcset="assets/images/foto@2x.png 2x, assets/images/foto@3x.png 3x">
                        <h3>Júlia Souza</h3>
                        <h4>destino: Helsinki, Finlândia</h4>
                        <p>
                            “A viagem foi ótima! O serviço oferecido pela Lojas Americanas foi perfeito. Não vemos a hora das próximas férias chegarem e prepararmos as malas de novo.”
                        </p>
                    </div>
                </div>
                
                <div class="slide">
                    <div class="content">
                        <img 
                            src="assets/images/foto.png"
                            srcset="assets/images/foto@2x.png 2x, assets/images/foto@3x.png 3x">
                        <h3>Júlia Souza</h3>
                        <h4>destino: Helsinki, Finlândia</h4>
                        <p>
                            “A viagem foi ótima! O serviço oferecido pela Lojas Americanas foi perfeito. Não vemos a hora das próximas férias chegarem e prepararmos as malas de novo.”
                        </p>
                    </div>
                </div>
                
                <div class="slide">
                    <div class="content">
                        <img 
                            src="assets/images/foto.png"
                            srcset="assets/images/foto@2x.png 2x, assets/images/foto@3x.png 3x">
                        <h3>Júlia Souza</h3>
                        <h4>destino: Helsinki, Finlândia</h4>
                        <p>
                            “A viagem foi ótima! O serviço oferecido pela Lojas Americanas foi perfeito. Não vemos a hora das próximas férias chegarem e prepararmos as malas de novo.”
                        </p>
                    </div>
                </div>
                
                <div class="slide">
                    <div class="content">
                        <img 
                            src="assets/images/foto.png"
                            srcset="assets/images/foto@2x.png 2x, assets/images/foto@3x.png 3x">
                        <h3>Júlia Souza</h3>
                        <h4>destino: Helsinki, Finlândia</h4>
                        <p>
                            “A viagem foi ótima! O serviço oferecido pela Lojas Americanas foi perfeito. Não vemos a hora das próximas férias chegarem e prepararmos as malas de novo.”
                        </p>
                    </div>
                </div>
                
                <div class="slide">
                    <div class="content">
                        <img 
                            src="assets/images/foto.png"
                            srcset="assets/images/foto@2x.png 2x, assets/images/foto@3x.png 3x">
                        <h3>Júlia Souza</h3>
                        <h4>destino: Helsinki, Finlândia</h4>
                        <p>
                            “A viagem foi ótima! O serviço oferecido pela Lojas Americanas foi perfeito. Não vemos a hora das próximas férias chegarem e prepararmos as malas de novo.”
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
    <?php include "includes/footer.php"; ?>

<!-- END CONTENT -->
<!-- END CONTENT -->

<script src="assets/vendor/jquery.min.js"></script>
<script src="assets/vendor/jquery-migrate.min.js"></script>

<!-- THIS COMMENT TAG IS NEEDED FOR GRUNT BUILD PROCESS -->
<!-- build:js assets/script/frontend.min.js -->
<!-- bower:js -->
<script src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
<script src="bower_components/slick-carousel/slick/slick.js"></script>
<!-- endbower -->
<script src="source/scripts/frontend.js"></script>
<!-- endbuild -->
</body>
</html>